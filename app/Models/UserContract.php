<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserContract extends Model
{
    use HasFactory;
    protected $table='user_contract';
    protected $fillable=['user_id','contract_id','contract_type','days_month','contract_start_date',
        'contract_end_date','delivery_duration','delivery_timing','e_sign_image','e_sign_image_path','contract_status','cancelled_time',
        'approved_time','rejected_time','reason','cancelled_by'];
}
