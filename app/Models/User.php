<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

  protected $table='users';
    protected $fillable = ['first_name','last_name','entity_type','city_id','email','password','country_code','mobile_no'
        ,'id_type','issuing_city_id','id_number','license_number','registration_number','manufacturer',
        'year_of_business','business_location','business_number','reference_number','email_verify','email_verify_key',
        'mobile_verify','attachment_1','attachment_1_path','attachment_1_type','attachment_2','attachment_2_path','attachment_2_type',
        'business_info_status','reason','latitude','longitude','user_language_code','otp','deleted_by',
        'status','approved_time','rejected_time'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
