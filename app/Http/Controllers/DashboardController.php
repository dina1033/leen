<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return response()->json([
            'total pending contract' => 4,
            'total pending order' => 3,
            'total pending waiting for pricing' => 5,
            'last 30 days contracts' => 5,
            'Total Item' => 5,
            'Total Category' => 5,
            'Total Order' => 5,
            'Total Contract Type' => 5,
            'Total City' => 5,
            'Total Active Contractor' => 5,
            'Total Inactive Contracts' => 5,
            'Total Driver' => 5,
            'TODAY ORDER & CONTRACT REGISTER' => 5,
            'WEEKLY ORDER' => 5,
            'LAST FIVE COMPLETE ORDER' => 5,
            'LAST FIVE ASSIGNED ORDER' => 5,
            'MONTHLY ORDER' => 5,
            'APP USER' => 5,

        ]);
    }
}
