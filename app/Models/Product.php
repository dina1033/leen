<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;


    protected $table='item';

    protected $fillable=['item_image','en_item_name','ar_item_name','en_description','ar_description','price',
        'price_per_qty','min_qty','status','price_unit','category_id','category_status','stock'];


    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
    public function orders(){
        return $this->belongsToMany('App\Models\Order','order_item', 'item_id', 'order_id')->withPivot('price');
    }
}
