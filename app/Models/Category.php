<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable=['category_image','en_category_name','ar_category_name','status','deleted_by'];

    protected $table='category';

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }
}
