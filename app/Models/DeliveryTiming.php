<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryTiming extends Model
{
    use HasFactory;
    protected $table='delivery_timing';
    protected $fillable=['en_delivery_timing','ar_delivery_timing'];
}
