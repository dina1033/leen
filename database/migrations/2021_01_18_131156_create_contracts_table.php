<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->id();
            $table->date('start_date');
            $table->date('end_date');
            $table->enum('delivery_duration',['Every Day','Every Two Days','Every Three Days']);
            $table->enum('delivery_duration_ar',['كل يوم','كل يومين','كل ثلاث ايام']);
            $table->enum('delivery_timing',['From 7 to 10 PM','From 8 to 10 AM','From 8 AM to 1 PM']);
            $table->enum('delivery_timing_ar',['بين 7 و 10 مساء','بين 8 و 10 صباحا','بين 8 صباحا و 1 ظهرا']);
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
