<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\OrderController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::get('/dashboard', [DashboardController::class,'index']);
Route::resource('/product', ProductController::class);
Route::get('/categoty_product/{category_id}', [ProductController::class,'getProductOfCategory']);
Route::resource('/category', CategoryController::class);
Route::resource('/city', CityController::class);
Route::resource('/country', CountryController::class);
Route::resource('/order', OrderController::class);

