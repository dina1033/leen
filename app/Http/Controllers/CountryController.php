<?php

namespace App\Http\Controllers;

use App\Http\Requests\CountryRequest;
use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function index()
    {
        $country=Country::where('deleted_at',null)->where('status','Active')->get();
        return response()->json($country);
    }


}
