<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'en_item_name' => 'required',
            'item_image' => 'required',
            'ar_item_name' => 'required',
            'en_description' => 'required',
            'ar_description' => 'required',
            'price' => 'required',
            'price_per_qty' => 'required',
            'min_qty' => 'required',
            'status' => 'required',
            'price_unit' => 'required',
            'category_id' => 'required',
            'category_status' => 'required',
            'stock' => 'required',
        ];
    }
}
