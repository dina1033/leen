<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;
    protected $table='page';
    protected $fillable=['page_slug','en_page_title','en_page_content','ar_page_content'];
}
