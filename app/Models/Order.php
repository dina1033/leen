<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $table='orders';
    protected $fillable=['user_contract_id','user_id','sub_total','vat','total_vat','total',
        'transaction_id','order_status','driver_id','driver_notes','user_notes','admin_notes','delivery_duration','delivery_timing',
        'schedule_date','completed_time','confirmed_time','on_way_time','cancelled_time',
        'assign_date','deleted_at'];

    public function products(){
        return $this->belongsToMany('App\Models\Product','order_item', 'order_id', 'item_id')->withPivot('price');
    }
    public function user(){
        return $this->belongsTo('App\Models\User','user_id');
    }
}
