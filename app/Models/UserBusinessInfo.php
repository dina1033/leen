<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBusinessInfo extends Model
{
    use HasFactory;
    protected $table='user_business_info';
    protected $fillable=['user_id','id_type','issuing_city_id','license_number','registration_number','manufacturer',
        'year_of_business','business_location','business_number','reference_number','id_number','attachment_1',
        'attachment_1_path','attachment_1_type','attachment_2','attachment_2_path','attachment_2_type',
        'status','business_info_status','latitude','longitude','approved_time','rejected_time','reason'];
}
