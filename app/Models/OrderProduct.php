<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    use HasFactory;
    protected $table='order_item';
    protected $fillable=['order_id','branch_id','item_id','qty','price','price_unit','price_per_qty'];
}
