<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SuggestionComplain extends Model
{
    use HasFactory;
    protected $table='suggestion_complain';
    protected $fillable=['user_id','title','active','details'];
}
