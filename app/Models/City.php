<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    protected $table='city';
    protected $fillable=['en_city_name','ar_city_name','deleted_by','status'];

}
