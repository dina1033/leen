<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Traits\SavingfileTrait;
class CategoryController extends Controller
{
    use SavingfileTrait;


    public function index()
    {
        $category=Category::where('status','Active')->where('deleted_at',null)->get();
        return response()->json($category);
    }

    public function store(CategoryRequest $request)
    {
        $filename=$this->saveFile($request->file('category_image'),public_path('storage/file'));
        $collection=collect($request->except('category_image'));
        $merge=$collection->merge(['category_image'=>$filename]);
        $category=new Category($merge->all());
        $category->image_path = 'public/storage/file';
        $category->save();
        return response()->json($category);
    }

    public function update(CategoryRequest $request, Category $category)
    {
        $category=Category::findOrFail($category->id);
        if($request->file('img')) {
            $file = $request->file('img');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move(public_path('storage/file'), $filename);
            $category->category_image = $filename;
        }
        else{
            $category->category_image = $category->category_image;
        }
        $category->en_category_name = $request->name;
        $category->ar_category_name = $request->name_ar;
        $category->image_path = $request->image_path;
        $category->deleted_by = $request->deleted_by;
        $category->status = $request->status;
        $category->save();
        return response()->json($category);
    }

    public function destroy(Category $category)
    {
        $category->delete();
        return response()->json($category);
    }
}
