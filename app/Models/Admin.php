<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    use HasFactory;

    protected $table='admin';

    protected $fillable=['user_type','first_name','last_name','email','password','image_path','profile_image','language_code','status'];
}
