<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryDuration extends Model
{
    use HasFactory;

    protected $table='delivery_duration';
    protected $fillable=['en_delivery_duration','ar_delivery_duration'];
}
