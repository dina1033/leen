<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Traits\SavingfileTrait;
class ProductController extends Controller
{
    use SavingfileTrait;


    public function index()
    {
        $product=Product::where('status','Active')->get();
        return response()->json($product);
    }

    public function store(ProductRequest $request)
    {
        $filename=$this->saveFile($request->file('item_image'),public_path('storage/file'));
        $collection=collect($request->except('item_image'));
        $merge=$collection->merge(['item_image'=>$filename]);
        $product=new Product($merge->all());
        $product->image_path = 'public/storage/file';
        $product->save();
        return response()->json($product);
    }

    public function update(ProductRequest $request, Product $product)
    {
        $product= Product::findOrFail($product->id);
        if($request->file('item_image')){
        $file = $request->file('item_image');
        $extension = $file->getClientOriginalExtension();
        $filename = time() . '.' . $extension;
        $file->move(public_path('storage/file'), $filename);
        $product->item_image = $filename;
        }else{
            $product->item_image = $product->item_image;
        }
        $product->image_path = 'public/storage/file';
        $product->en_item_name = $request->en_item_name;
        $product->ar_item_name = $request->ar_item_name;
        $product->en_description = $request->en_description;
        $product->ar_description = $request->ar_description;
        $product->price = $request->price;
        $product->price_per_qty = $request->price_per_qty;
        $product->min_qty = $request->min_qty;
        $product->status = $request->status;
        $product->price_unit = $request->price_unit;
        $product->category_id = $request->category_id;
        $product->category_status = $request->category_status;
        $product->stock = $request->stock;
        $product->save();

        return response()->json($product);
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return response()->json($product);
    }

    public function getProductOfCategory($category_id){
        $product=Product::where('category_id',$category_id)->where('deleted_at',null)
            ->where('category_status','Active')->where('status','Active')->get();
        return response()->json($product);
    }


}
