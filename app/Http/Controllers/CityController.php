<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function index()
    {
        $city=City::where('status','Active')->where('deleted_at',null)->get();
        return response()->json($city);
    }

    public function store(Request $request)
    {
        $city=City::Create($request->all());

        return response()->json($city);
    }

    public function update(Request $request, City $city)
    {
        $city->update($request->all());
        return response()->json($city);
    }

    public function destroy(City $city)
    {
        $city->delete();
        return response()->json($city);
    }
}
